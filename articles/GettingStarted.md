# Using max.serialize

``max.serialize`` is a MaxLib library that makes it easy to serialize objects into various string formats, such as XML, JSON and YAML.

* [Installation](Installation.md)
* [Making Objects Serializable](Serializable.md)
* [Serialization](Serialization.md)
* [Deserialization](Deserialization.md)

## Max Dependencies:

``max.serialize`` does not depend on any other Max libraries.