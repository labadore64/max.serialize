# Making Objects Serializable

## Getting Started

Assume that our class is something like this:

```
public class Person
{
  public string Name { get; set; }
  public int Age { get; set; }
  public string[] ChildrenNames { get; set; }
  public SomeSerializableClass CoolObj { get; set; }
  public Vector2 Point { get; set; }

  // returns all the members in a string
    public override string ToString()
    {
    string ReturnString; = Name + ", Age: " + Age;
    if(ChildrenNames != null){
      for(int i = 0; i < ChildrenNames; i++){
        ReturnString += ", Child " + i + ": " + ChildrenNames[i];
      }
    }
    if(CoolObj != null){
      ReturnString += CoolObj.ToString();
    }
    ReturnString += "(x : "+ 
            Point.X + 
            " y : " + 
            Point.Y + " )";
        return ReturnString;
    }
}

```

By implementing the ``IMaxSerializable`` interface and its methods, we can enable this object to be serialized/deserialized by the classes in this library.

## Serialization

Any object that implements ``IMaxSerializable`` will inherit the method ``MaxSerializedObject Serialize()``. This method deserializes a ``MaxSerializedObject`` from data. The developer defines what values are placed into the ``MaxSerializedObject``.

### Initial Setup

To start, the ``Serialize`` method should have a basic empty definition like this:

```
MaxSerializedObject Data = MaxSerializedObject.Create();
return data;
```

This will just return empty data for the serializer.

### Serializing Primatives, Strings, and Arrays

You can add primatives simply by doing the following:

```
// ...

data.AddString("Name", Name);
data.AddInt("Age", Age);
data.AddFloat(Distance,15.48);

// ...
```

Currently supported types are ``char``, ``bool``, ``byte``, ``int``, ``float``, ``long``, ``double`` and ``string``.

Some XNA types are supported as well:

```
// ...

data.AddVector2("Point", Point);

/// ...
```

Supported types are ``Vector2``, ``Vector3`` and ``Keys``.

### Serializing Serializable Objects

You can serialize other serializable objects by serializing them into an object and adding it to the Dictionary.

```
// ...

data.AddSerializedObject("CoolObj", CoolObj);

/// ...
```

You can also serialize collections of serializable objects as well.

```

/// ...

data.AddSerializedObjectCollection("CoolObjects", new List<CoolObj>());

/// ...

```

## Deserialization

Any object that implements ``IMaxSerializable`` will inherit the method ``void Deserialize(MaxSerializedObject Data)``. This method deserialized the passed ``MaxSerializedObject`` to the appropriate fields.

### Initial Setup

The default ``Deserialize`` method can just be an empty method. Values are extracted from the passed argument.

### Constructor

It's a good idea to add constructors that can input serialized data so that it can deserialize the data while instantiating the object.

```
public Person(MaxSerializedObject Data)
{
    Deserialize(Data);
}
```

### Deserializing Primatives, Strings, and Arrays

You can deserialize primatives by first converting them to strings, then parsing them as the approrpiate types.

```
// ...

Name = data.GetString("Name");
Age = data.GetInt("Age");
Distance = data.GetFloat(Distance);

// ...
```

If a key is not found, it will be set to an empty or 0 value.

Currently supported types are ``char``, ``bool``, ``byte``, ``int``, ``float``, ``long``, ``double`` and ``string``.

Some XNA types are supported as well:

```
// ...

Point = data.GetVector2("Point");

/// ...
```

### Deserializing Serializable Objects

You can deserialize other serializable objects. 

```

CoolObj = (CoolObj)data.GetSerializedObject("CoolObj").Deserialize();

```