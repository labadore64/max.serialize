# Serialization

Serialization converts your serialized objects as ``MaxSerializedObject`` and converts them into several possible text formats.

The supported text formats are:

* XML
* YAML
* JSON

After [making your object serializable](Serializable.md), you can serialize your object by calling the appropriate serializer method from ``MaxSerializer``.

```
// Serializes CoolObj into an XML string.
string SerializedXMLString = MaxSerializer.SerializeXML(CoolObj);

// Serializes CoolObj into a YAML string.
string SerializedYamlString = MaxSerializer.SerializeYAML(CoolObj);

// Serializes CoolObj into a JSON string.
string SerializedJsonString = MaxSerializer.SerializeJSON(CoolObj);
```

You can also serialize ``MaxSerializedObject``:

```
MaxSerializedObject Data = MaxSerializedObject.Create();

// ...
// Add stuff to the MaxSerializedObject
// ...

// Serializes CoolObj into an XML string.
string SerializedXMLString = MaxSerializer.SerializeXML(Data);

// Serializes CoolObj into a YAML string.
string SerializedYamlString = MaxSerializer.SerializeYAML(Data);

// Serializes CoolObj into a JSON string.
string SerializedJsonString = MaxSerializer.SerializeJSON(Data);
```
