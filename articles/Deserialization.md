# Deserialization

Deserialization converts strings in several text formats into serialized objects in ``MaxSerializedObject`` form.

The supported text formats are:

* XML
* YAML
* JSON

After [making your object serializable](Serializable.md), you can deserialize your string by calling the appropriate deserializer method from ``MaxSerializer``, which will convert the string into an ``MaxSerializedObject`` object.

```
MaxSerializedObject MaxSerialized;

// Deserializes XML 
MaxSerialized = MaxSerializer.DeserializeXML(CoolObj);

// Deserializes YAML
MaxSerialized = MaxSerializer.DeserializeYAML(CoolObj);

// Deserializes JSON
MaxSerialized = MaxSerializer.DeserializeJSON(CoolObj);
```

To deserialize the object completely, call ``MaxSerializedObject.Deserialize()`` and cast the object to the proper type.

```
CoolObj = (Person)MaxSerialized.Deserialize();
```