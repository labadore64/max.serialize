# max.serialize

``max.serialize`` is a MaxLib Helper Library that makes it easy to serialize objects into various string formats, such as XML, JSON and YAML.

* [Documentation](https://labadore64.gitlab.io/max.serialize/)
* [Source](https://gitlab.com/labadore64/max.serialize/)