﻿using System.Collections.Generic;

namespace max.serialize
{
    /// <summary>
    /// This interface gives objects the ability to be serialized/deserialized
    /// by max.serialize.
    /// </summary>
    public interface IMaxSerializable
    {
        /// <summary>
        /// Returns the object serialized into a Dictionary. The
        /// string should correlate with the key in the serialized file, and the
        /// object should correlate with the primative value, or a serialized object.
        /// </summary>
        /// <returns>The serialized object.</returns>
        MaxSerializedObject Serialize();
        /// <summary>
        /// Deserializes the dictionary into the object, setting
        /// its properies based on the values in the serialized
        /// object. It's a good idea to include a constructor that
        /// has a Dictionary parameter so you can
        /// deserialize the object when instantiating.
        /// </summary>
        /// <param name="Data">The serialized object</param>
        void Deserialize(MaxSerializedObject Data);

        /// <summary>
        /// Whether or not this component will be serialized.
        /// If false, this object will be ignored in serialization methods.
        /// </summary>
        /// <value>True/False</value>
        bool Serializable { get; set; }

        /// <summary>
        /// Creates an instance of this type from the serialized data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized object</returns>
        IMaxSerializable CreateInstance(MaxSerializedObject Data);

    }
}
