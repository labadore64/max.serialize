﻿using max.serialize.type;
using max.util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace max.serialize
{
    /// <summary>
    /// Object data serialized into an object.
    /// </summary>
    public class MaxSerializedObject
    {
        #region Constants

        /// <summary>
        /// The string key for types.
        /// </summary>
        /// <value>"type"</value>
        public const string SER_TYPE = "type";

        #endregion

        #region Indexer

        /// <summary>
        /// Gets data from its string key.
        /// </summary>
        /// <param name="index">Index</param>
        /// <returns>Data</returns>
        public object this[string index]
        {
            get
            {
                // get the item for that index.
                return Data[index];
            }
            set
            {
                // set the item for this index. value will be of type Thing.
                AddToData(index, value);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Contains the data of the serialized object.
        /// </summary>
        /// <value>Data</value>
        public Dictionary<string, object> Data { get; protected set; } = new Dictionary<string, object>();

        Type Type = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates an empty serialized object.
        /// </summary>
        public MaxSerializedObject()
        {

        }

        /// <summary>
        /// Creates a serialized object from dictionary data passed as an object.
        /// </summary>
        /// <param name="Data">Data</param>
        public MaxSerializedObject(object Data)
        {
            if (Data.GetType() == typeof(MaxSerializedObject))
            {
                this.Data = SerializeRecursive((MaxSerializedObject)Data);
            } else
            {
                this.Data = SerializeRecursive((Dictionary<string,object>)Data);
            }
        }

        /// <summary>
        /// Creates a serialized object from dictionary data passed as an object.
        /// </summary>
        /// <param name="Data">Data</param>
        public MaxSerializedObject(Dictionary<string, object> Data)
        {
            this.Data = SerializeRecursive(Data);
        }

        // serializes the data in the MaxSerializedObject and returns the dictionary.
        private Dictionary<string,object> SerializeRecursive(MaxSerializedObject Data)
        {
            return SerializeRecursive(Data.Data);
        }

        // serializes the objects recursively in the dictionary and returns the fixed
        // dictionary.
        private Dictionary<string, object> SerializeRecursive(Dictionary<string, object> data)
        {
            Dictionary<string, object> dataList = new Dictionary<string, object>();


            foreach (KeyValuePair<string, object> d in data)
            {
                if (d.Value is Dictionary<string, object>)
                {
                    // if the value is a dictionary
                    dataList.Add(d.Key, new MaxSerializedObject(d.Value));
                }
                else
                {
                    // default case.
                    dataList.Add(d.Key, d.Value);
                }
            }

            return dataList;
        }

        #endregion

        #region Add functions

        #region serialized objects
        /// <summary>
        /// Add a serialized object to data.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Object">Serialized object</param>
        public void AddSerializedObject(string ID, MaxSerializedObject Object)
        {
            AddToData(ID, Object);
        }

        /// <summary>
        /// Add a serialized object to data.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Object">Object</param>
        public void AddSerializedObject(string ID, IMaxSerializable Object)
        {
            if (Object.Serializable)
            {
                AddToData(ID, Object.Serialize());
            }
        }

        /// <summary>
        /// Add a collection of serialized objects.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Objects">Objects</param>
        public void AddSerializedObjectCollection(string ID, ICollection<MaxSerializedObject> Objects)
        {
            MaxSerializedObject[] objects = new MaxSerializedObject[Objects.Count];
            Objects.CopyTo(objects, 0);
            AddToData(ID, objects);
        }

        /// <summary>
        /// Add an array of serializable objects.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Objects">Objects</param>
        public void AddSerializableObjectCollection(string ID, IMaxSerializable[] Objects)
        {
            MaxSerializedObject[] objects = new MaxSerializedObject[Objects.Length];
            for (int i = 0; i < objects.Length; i++)
            {
                if (Objects[i].Serializable)
                {
                    objects[i] = Objects[i].Serialize();
                }
            }
            AddToData(ID, objects);
        }

        /// <summary>
        /// Adds a collection of serializable objects to the collection.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Collection">Collection to add</param>
        public void AddSerializableObjectCollection(string ID, ICollection<IMaxSerializable> Collection)
        {
            MaxSerializedObject[] objects = new MaxSerializedObject[Collection.Count];
            IMaxSerializable[] coll = new IMaxSerializable[Collection.Count];
            Collection.CopyTo(coll, 0);
            for (int i = 0; i < objects.Length; i++)
            {
                if (coll[i].Serializable)
                {
                    coll[i].Serialize();
                }
            }
            AddToData(ID, objects);
        }

        /// <summary>
        /// Adds a collection of objects to the collection.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Collection">Collection to add</param>
        public void AddCollection(string ID, ICollection Collection)
        {
            Type[] types = Collection.GetType().GetGenericArguments();

            object[] objects = new IMaxSerializable[Collection.Count];
            Collection.CopyTo(objects, 0);

            if (types.Length > 0)
            {
                Type type = types[0];

                for (int i = 0; i < objects.Length; i++)
                {
                    if (type == typeof(string))
                    {
                        objects[i] = (string)objects[i];
                    }
                    else if (type == typeof(int))
                    {
                        objects[i] = (int)objects[i];
                    }
                    else if (type == typeof(long))
                    {
                        objects[i] = (long)objects[i];
                    }
                    else if (type == typeof(float))
                    {
                        objects[i] = (float)objects[i];
                    }
                    else if (type == typeof(double))
                    {
                        objects[i] = (double)objects[i];
                    }
                    else if (type == typeof(byte))
                    {
                        objects[i] = (int)objects[i];
                    }
                    else if (type == typeof(char))
                    {
                        objects[i] = (char)objects[i];
                    } 
                }
            }

            AddToData(ID, objects);
        }

        #endregion

        #region XNA components

        /// <summary>
        /// Adds a Vector3 to the serialized object.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Vector2">Point</param>
        public void AddVector2(string ID, Vector2 Vector2)
        {
            AddToData(ID, Vector2Serializer.Serialize(Vector2));
        }

        /// <summary>
        /// Adds a collection of Vector2 to the serialized object.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Collection">Collection of points</param>
        public void AddVector2Collection(string ID, ICollection<Vector2> Collection)
        {
            List<MaxSerializedObject> objs = new List<MaxSerializedObject>();
            Vector2[] vec2 = new Vector2[Collection.Count];
            Collection.CopyTo(vec2, 0);
            for(int i = 0; i < vec2.Length; i++)
            {
                objs.Add(Vector2Serializer.Serialize(vec2[i]));
            }

            AddSerializedObjectCollection(ID, objs);
        }

        /// <summary>
        /// Adds a Vector3 to the serialized object.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Vector3">Point</param>
        public void AddVector3(string ID, Vector3 Vector3)
        {
            AddToData(ID, Vector3Serializer.Serialize(Vector3));
        }

        /// <summary>
        /// Adds a collection of Vector3 to the serialized object.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Collection">Collection of points</param>
        public void AddVector3Collection(string ID, ICollection<Vector3> Collection)
        {
            List<MaxSerializedObject> objs = new List<MaxSerializedObject>();
            Vector3[] vec3 = new Vector3[objs.Count];
            Collection.CopyTo(vec3, 0);
            for (int i = 0; i < vec3.Length; i++)
            {
                objs.Add(Vector3Serializer.Serialize(vec3[i]));
            }

            AddSerializedObjectCollection(ID, objs);
        }

        /// <summary>
        /// Adds a keyboard key to the serialized object.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Key">Key</param>
        public void AddKey(string ID, Keys Key)
        {
            AddString(ID, Key.ToString());
        }

        /// <summary>
        /// Adds a set of keyboard keys to the serialized object.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Keys">Keys</param>
        public void AddKeyCollection(string ID, ICollection<Keys> Keys)
        {
            string[] keys = new string[Keys.Count];
            Keys[] keyValues = new Keys[Keys.Count];
            Keys.CopyTo(keyValues, 0);
            for(int i = 0; i < keys.Length; i++)
            {
                keys[i] = keyValues[i].ToString();
            }

            AddCollection(ID, keys);
        }

        #endregion

        #region Primatives
        /// <summary>
        /// Sets int in data using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Integer">Integer</param>
        public void AddInt(string ID, int Integer)
        {
            AddToData(ID, Integer);
        }
        /// <summary>
        /// Sets byte in data using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Byte">Byte</param>
        public void AddByte(string ID, byte Byte)
        {
            AddToData(ID, Byte);
        }
        /// <summary>
        /// Sets long in data using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Long">Long</param>
        public void AddLong(string ID, long Long)
        {
            AddToData(ID, Long);
        }
        /// <summary>
        /// Sets float in data using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Float">Float</param>
        public void AddFloat (string ID, float Float)
        {
            AddToData(ID, Float);
        }/// <summary>
         /// Sets double in data using the ID as a key.
         /// </summary>
         /// <param name="ID">Accessor ID</param>
         /// <param name="Double">Double</param>
        public void AddDouble(string ID, double Double)
        {
            AddToData(ID, Double);
        }
        /// <summary>
        /// Sets char in data using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Char">Character</param>
        public void AddChar(string ID, char Char)
        {
            AddToData(ID, Char);
        }
        /// <summary>
        /// Sets string in data using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="String">String</param>
        public void AddString(string ID, string String)
        {
            AddToData(ID, String);
        }

        /// <summary>
        /// Sets boolean in data using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Boolean">Boolean</param>
        public void AddBoolean(string ID, bool Boolean)
        {
            if (Boolean)
            {
                AddToData(ID, "true");
            } else
            {
                AddToData(ID, "false");
            }
        }

        // Adds data to the data dictionary
        private void AddToData(string ID, object obj)
        {
            if (Data.ContainsKey(ID))
            {
                Data[ID] = obj;
            } else
            {
                Data.Add(ID,obj);
            }
        }

        #endregion

        #region Reflection
        /// <summary>
        /// Adds the full type name string using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Type">Type</param>
        public void AddFullType(string ID, Type Type)
        {
            AddString(ID, ReflectionUtil.FullTypeToString(Type));
        }

        /// <summary>
        /// Adds the type name string using the ID as a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <param name="Type">Type</param>
        public void AddType(string ID, Type Type)
        {
            AddString(ID, ReflectionUtil.TypeToString(Type));
        }

        /// <summary>
        /// Adds the full type name and method using the ID as a key.
        /// </summary>
        /// <param name="ID">Key</param>
        /// <param name="Method">Method</param>
        public void AddFullMethod(string ID, MethodInfo Method)
        {
            AddString(ID, ReflectionUtil.FullMethodToString(Method));
        }

        #endregion

        #endregion

        #region Get Methods

        #region serialized objects
        /// <summary>
        /// Get a serialized object from an ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Serialized object</returns>
        public MaxSerializedObject GetSerializedObject(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return (MaxSerializedObject)Data[ID];
            }
            return null;
        }
        /// <summary>
        /// Returns a collection of MaxSerializedObjects from a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Collection</returns>
        public List<MaxSerializedObject> GetSerializedObjectCollection(string ID)
        {
            List<MaxSerializedObject> objects = new List<MaxSerializedObject>();
            if (Data.ContainsKey(ID))
            {
                object[] Links = (object[])Data[ID];
                for (int i = 0; i < Links.Length; i++)
                {
                    if (Links[i] is MaxSerializedObject) {
                        objects.Add((MaxSerializedObject)Links[i]);
                    } else
                    {
                        objects.Add(new MaxSerializedObject(Links[i]));
                    }
                }
            }

            return objects;
        }

        /// <summary>
        /// Returns a collection of objects of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Collection</returns>
        public List<object> GetCollection(string ID)
        {
            List<object> objects = new List<object>();

            if (Data.ContainsKey(ID))
            {
                if (Data[ID] is object[])
                {
                    object[] objarray = (object[])Data[ID];
                    objects = new List<object>(objarray);
                }
            }

            return objects;
        }

        #endregion

        #region XNA components
        /// <summary>
        /// Get a Vector2 of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Vector2</returns>
        public Vector2 GetVector2(string ID)
        {
            return Vector2Serializer.Deserialize(GetSerializedObject(ID));
        }

        /// <summary>
        /// Get an array of Vector2 of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Vector2 array</returns>
        public List<Vector2> GetVector2List(string ID)
        {
            List<MaxSerializedObject> objs= GetSerializedObjectCollection(ID);
            List<Vector2> points = new List<Vector2>();
            for(int i = 0; i < objs.Count; i++)
            {
                points.Add(Vector2Serializer.Deserialize(objs[i]));
            }

            return points;
        }
        
        /// <summary>
        /// Get a Vector3 of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Vector3</returns>
        public Vector3 GetVector3(string ID)
        {
            return Vector3Serializer.Deserialize(GetSerializedObject(ID));
        }

        /// <summary>
        /// Get an array of Vector3 of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Vector3 array</returns>
        public List<Vector3> GetVector3List(string ID)
        {
            List<MaxSerializedObject> objs = GetSerializedObjectCollection(ID);
            List<Vector3> points = new List<Vector3>();
            for (int i = 0; i < objs.Count; i++)
            {
                points.Add(Vector3Serializer.Deserialize(objs[i]));
            }

            return points;
        }

        /// <summary>
        /// Gets a key of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Key</returns>
        public Keys GetKey(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return (Keys)Enum.Parse(typeof(Keys), GetString(ID));
            }

            return Keys.A;
        }

        /// <summary>
        /// Gets a list of keys of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Keys</returns>
        public List<Keys> GetKeysList(string ID)
        {
            object[] objs = (object[])Data[ID];
            List<Keys> keys = new List<Keys>();
            for (int i = 0; i < objs.Length; i++)
            {
                keys.Add((Keys)Enum.Parse(typeof(Keys), (string)objs[i]));
            }

            return keys;
        }

        #endregion

        #region Primatives
        /// <summary>
        /// Get an int of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Integer</returns>
        public int GetInt(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return int.Parse((string)Data[ID]);
            }
            return 0;
        }
        /// <summary>
        /// Get a byte of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>byte</returns>
        public byte GetByte(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return byte.Parse((string)Data[ID]);
            }
            return 0;
        }
        /// <summary>
        /// Get a long of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>long</returns>
        public long GetLong(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return long.Parse((string)Data[ID]);
            }
            return 0;
        }
        /// <summary>
        /// Get a float of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Float</returns>
        public float GetFloat(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return float.Parse((string)Data[ID]);
            }
            return 0;
        }
        /// <summary>
        /// Get a double of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Double</returns>
        public double GetDouble(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return double.Parse((string)Data[ID]);
            }
            return 0;
        }
        /// <summary>
        /// Get a character value of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Character</returns>
        public char GetChar(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return char.Parse((string)Data[ID]);
            }
            return ' ';
        }
        /// <summary>
        /// Get the string value of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>String</returns>
        public string GetString(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return (string)Data[ID];
            }
            return "";
        }
        /// <summary>
        /// Gets the boolean value of a specific ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>True/False</returns>
        public bool GetBoolean(string ID)
        {
            if (Data.ContainsKey(ID))
            {
                return (bool)Data[ID];
            }
            return false;
        }

        #endregion

        #region Reflection
        /// <summary>
        /// Gets the method from the full method string in the ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Method</returns>
        public MethodInfo GetFullMethod(string ID)
        {
            return ReflectionUtil.FullStringToMethod(GetString(ID));
        }

        /// <summary>
        /// Gets the type from the full type string in the ID.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>Type</returns>
        public Type GetFullType(string ID)
        {
            return ReflectionUtil.FullStringToType(GetString(ID));
        }
        #endregion

        #endregion

        #region ContainsKey

        /// <summary>
        /// Returns if the object contains a key.
        /// </summary>
        /// <param name="ID">Accessor ID</param>
        /// <returns>True/False</returns>
        public bool ContainsKey(string ID)
        {
            return Data.ContainsKey(ID);
        }
        #endregion

        #region Type functions
        /// <summary>
        /// Sets the type of the serialized object.
        /// </summary>
        /// <param name="Type">Type</param>
        public void SetType(Type Type)
        {
            this.Type = Type;
            AddString(SER_TYPE, ReflectionUtil.FullTypeToString(Type));
        }

        /// <summary>
        /// Sets the type of the serialized object.
        /// </summary>
        /// <param name="Object">Object</param>
        public void SetType(object Object)
        {
            Type = Object.GetType();
            AddString(SER_TYPE, ReflectionUtil.FullTypeToString(Type));
        }

        /// <summary>
        /// Gets the type of the serialized object.
        /// </summary>
        /// <returns>Type</returns>
        public Type GetSerializedType()
        {
            if (Type == null)
            {
                Type = ReflectionUtil.FullStringToType(GetString(SER_TYPE));
            }
            return Type;
        }
        #endregion

        #region Deserialize

        /// <summary>
        /// Returns the deserialized object.
        /// </summary>
        /// <returns>Deserialized object</returns>
        public IMaxSerializable Deserialize()
        {
            if(Type == null)
            {
                Type = ReflectionUtil.FullStringToType(GetString(SER_TYPE));
            }

            // if the type exists
            if (Type != null)
            {
                IMaxSerializable obj;

                IMaxSerializable typ = Serializables.Find(x => x.GetType() == Type);

                if (typ != null)
                {
                    obj = typ.CreateInstance(this);
                }
                else
                {
                    obj = (IMaxSerializable)Activator.CreateInstance(Type);

                    // deserialize its contents
                    obj.Deserialize(this);
                }

                return obj;
            }

            throw new Exception("The type cannot be null.");
        }

        #endregion

        #region Static methods

        // list of serializable objects
        static List<IMaxSerializable> Serializables = new List<IMaxSerializable>();

        /// <summary>
        /// Initializes the types used in deserialization. This optimizes deserialization by reducing reflection calls.
        /// When deserializing a MaxSerializedObject, instead of creating a new instance through reflection,
        /// it just uses the object's CreateInstance() method.
        /// </summary>
        public static void InitializeTypes()
        {
            Serializables.Clear();
            List<Type> types = ReflectionUtil.GetTypesContainingType(typeof(IMaxSerializable)).FindAll(x => x.IsClass && !x.IsAbstract);
            for(int i = 0; i < types.Count; i++)
            {
                Serializables.Add((IMaxSerializable)Activator.CreateInstance(types[i]));
            }
        }

        /// <summary>
        /// Creates an instance of MaxSerializedObject.
        /// </summary>
        /// <returns>New instance of MaxSerializedObject.</returns>
        public static MaxSerializedObject Create()
        {
            return new MaxSerializedObject();
        }
        #endregion
    }
}
