﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace max.serialize.type
{
    /// <summary>
    /// A static class that serializes Vector2 for max.serialize
    /// </summary>
    public static class Vector2Serializer
    {
        const string SER_X = "x";
        const string SER_Y = "y";

        /// <summary>
        /// Deserializes the data into Vector2.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Vector2</returns>
        public static Vector2 Deserialize(MaxSerializedObject Data)
        {
            return new Vector2(Data.GetFloat(SER_X), Data.GetFloat(SER_Y));
        }

        /// <summary>
        /// Serializes the Vector2.
        /// </summary>
        /// <param name="Vector">Vector2</param>
        /// <returns>Serialized object</returns>
        public static MaxSerializedObject Serialize(Vector2 Vector)
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            Data.AddFloat(SER_X, Vector.X);
            Data.AddFloat(SER_Y, Vector.Y);
            return Data;
        }
    }
}
