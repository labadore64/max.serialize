﻿using Microsoft.Xna.Framework;

namespace max.serialize.type
{
    /// <summary>
    /// A static class that serializes Vector3 for max.serialize
    /// </summary>
    public static class Vector3Serializer
    {
        const string SER_X = "x";
        const string SER_Y = "y";
        const string SER_Z = "z";

        /// <summary>
        /// Deserializes the data into Vector3.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Vector3</returns>
        public static Vector3 Deserialize(MaxSerializedObject Data)
        {
            return new Vector3(Data.GetFloat(SER_X), Data.GetFloat(SER_Y), Data.GetFloat(SER_Z));
        }

        /// <summary>
        /// Serializes the Vector3.
        /// </summary>
        /// <param name="Vector">Vector3</param>
        /// <returns>Serialized object</returns>
        public static MaxSerializedObject Serialize(Vector3 Vector)
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            Data.AddFloat(SER_X, Vector.X);
            Data.AddFloat(SER_Y, Vector.Y);
            Data.AddFloat(SER_Z, Vector.Z);
            return Data;
        }
    }
}
