﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using YamlDotNet.Serialization;

namespace max.serialize
{
    /// <summary>
    /// This class serializes object data into text. Objects must implement MaxSerializable
    /// to be serialized/deserialized by this class.
    /// </summary>
    public static class MaxSerializer
    {
        /// <summary>
        /// The different text serialization types.
        /// </summary>
        public enum SerialFormat { 
            /// <summary>
            /// XML format
            /// </summary>
            XML, 
            /// <summary>
            /// JSON Format
            /// </summary>
            JSON, 
            /// <summary>
            /// YAML Format
            /// </summary>
            YAML 
        };

        #region Serialize
        /// <summary>
        /// Serializes data into JSON.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>JSON String</returns>
        public static string SerializeJSON(MaxSerializedObject Data)
        {
            return JsonConvert.SerializeObject(Data, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Serialize an object into JSON.
        /// </summary>
        /// <param name="Object">MaxSerializable object</param>
        /// <returns>JSON String</returns>
        public static string SerializeJSON(IMaxSerializable Object)
        {
            return JsonConvert.SerializeObject(Object.Serialize(), Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Serializes data into XML.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>XML String</returns>
        public static string SerializeXML(MaxSerializedObject Data)
        {
            XNode node = JsonConvert.DeserializeXNode(SerializeJSON(Data), "Root");
            return node.ToString();
        }

        /// <summary>
        /// Serialize an object into XML.
        /// </summary>
        /// <param name="Object">MaxSerializable object</param>
        /// <returns>XML String</returns>
        public static string SerializeXML(IMaxSerializable Object)
        {
            XNode node = JsonConvert.DeserializeXNode(SerializeJSON(Object.Serialize()), "Root");
            return node.ToString();
        }

        /// <summary>
        /// Serializes data into XML.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <param name="Root">Root Node</param>
        /// <returns>XML String</returns>
        public static string SerializeXML(MaxSerializedObject Data, string Root)
        {
            XNode node = JsonConvert.DeserializeXNode(SerializeJSON(Data), Root);
            return node.ToString();
        }

        /// <summary>
        /// Serialize an object into XML.
        /// </summary>
        /// <param name="Object">MaxSerializable object</param>
        /// <param name="Root">Root Node</param>
        /// <returns>XML String</returns>
        public static string SerializeXML(IMaxSerializable Object, string Root)
        {
            XNode node = JsonConvert.DeserializeXNode(SerializeJSON(Object.Serialize()), Root);
            return node.ToString();
        }

        /// <summary>
        /// Serializes data into YAML.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>YAML String</returns>
        public static string SerializeYAML(MaxSerializedObject Data)
        {
            var serializer = new SerializerBuilder().Build();
            return serializer.Serialize(SerializeConvertMaxSerializedObject(Data));
        }

        /// <summary>
        /// Serialize an object into YAML.
        /// </summary>
        /// <param name="Object">MaxSerializable object</param>
        /// <returns>YAML String</returns>
        public static string SerializeYAML(IMaxSerializable Object)
        {
            MaxSerializedObject Data = Object.Serialize();
            var serializer = new SerializerBuilder().Build();
            return serializer.Serialize(SerializeConvertMaxSerializedObject(Data));
        }

        // Turns a serialized object into Dictionary<string,object> data and cleans it too.
        private static Dictionary<string,object> SerializeConvertMaxSerializedObject(MaxSerializedObject Data)
        {
            bool updated;
            for (int count = 0; count < Data.Data.Count; count++)
            {
                updated = false;
                var element = Data.Data.ElementAt(count);
                if(element.Value is MaxSerializedObject)
                {
                    MaxSerializedObject obj = (MaxSerializedObject)element.Value;
                    if (obj != null)
                    {
                        Data[element.Key] = SerializeConvertMaxSerializedObject(obj);
                        updated = true;
                    }
                } else if (element.Value is MaxSerializedObject[])
                {
                    MaxSerializedObject[] objects = (MaxSerializedObject[])element.Value;
                    List<Dictionary<string, object>> datas = new List<Dictionary<string, object>>();

                    for(int j = 0; j < objects.Length; j++)
                    {
                        if (objects[j] != null)
                        {
                            datas.Add(SerializeConvertMaxSerializedObject(objects[j]));
                            updated = true;
                        }
                    }
                    Data[element.Key] = datas.ToArray();
                } else
                {
                    updated = true;
                }
                if (!updated)
                {
                    Data.Data.Remove(element.Key);
                    count--;
                }
            }

            return Data.Data;
        }

        #endregion

        #region Deserialize

        /// <summary>
        /// Deserializes JSON text as a Dictionary.
        /// </summary>
        /// <param name="JSON">JSON Text</param>
        /// <returns>Object data</returns>
        public static MaxSerializedObject DeserializeJSON(String JSON)
        {
            return DeserializeDictionary(JsonConvert.DeserializeObject<Dictionary<string, object>>(JSON));
        }

        // Deserializes a JSON string
        private static MaxSerializedObject DeserializeJSON(JObject JSON)
        {
            return DeserializeDictionary(JsonConvert.DeserializeObject<Dictionary<string, object>>(JSON.ToString()));
        }

        // Deserializes objects in a Dictionary recursively.
        private static MaxSerializedObject DeserializeDictionary(Dictionary<string, object> Dictionary)
        {
            // get the keys of the object
            string[] keys = new string[Dictionary.Keys.Count];

            // get the values of the object
            object[] values = new object[Dictionary.Values.Count];

            for (int i = 0; i < Dictionary.Count; i++)
            {
                // if the object is of type JObject
                if (values[i].GetType() == typeof(JObject))
                {
                    // Cast it as a JObject and deserialize
                    Dictionary[keys[i]] = DeserializeJSON((JObject)values[i]);
                }
                // if the object is of type JArray
                else if (values[i].GetType() == typeof(JArray))
                {
                    // Cast it as a JArray and deserialize
                    Dictionary[keys[i]] = DeserializeJSON((JArray)values[i]);
                }
            }

            return new MaxSerializedObject(Dictionary);
        }

        // Deserializes JSON arrays.
        private static object DeserializeJSON(JArray JSON)
        {
            object[] deserialized = JsonConvert.DeserializeObject<object[]>(JSON.ToString());

            for (int i = 0; i < deserialized.Length; i++)
            {
                // if the object is of type JObject
                if (deserialized[i].GetType() == typeof(JObject))
                {
                    // Deserialize the JSON as an object
                    deserialized[i] = DeserializeJSON((JObject)deserialized[i]);
                }
                // if the object is of type JArray
                else if (deserialized[i].GetType() == typeof(JArray))
                {
                    // Deserialize the JSON as an array
                    deserialized[i] = DeserializeJSON((JArray)deserialized[i]);
                }
            }

            // return the deserialized object
            return deserialized;
        }

        /// <summary>
        /// Deserializes XML text as a Dictionary.
        /// </summary>
        /// <param name="XML">XML Text</param>
        /// <returns>Object data</returns>
        public static MaxSerializedObject DeserializeXML(string XML)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(XML);
            return CleanData(DeserializeJSON(JsonConvert.SerializeXmlNode(doc.FirstChild.FirstChild)).Data);
        }

        /// <summary>
        /// Deserializes YAML text as a Dictionary.
        /// </summary>
        /// <param name="YAML">YAML Text</param>
        /// <returns>Object data</returns>
        public static MaxSerializedObject DeserializeYAML(string YAML)
        {
            var deserializer = new Deserializer();
            if (YAML.Length > 0)
            {
                return CleanData(deserializer.Deserialize<Dictionary<string, object>>(YAML));
            }

            return MaxSerializedObject.Create();
        }

        #endregion

        #region Internal

        // Cleans data in deserializing so that types are consistent among all serialization types
        private static MaxSerializedObject CleanData(Dictionary<string, object> Data)
        {
            if (Data != null)
            {
                string[] keys = new string[Data.Keys.Count];
                Data.Keys.CopyTo(keys, 0);
                object[] values = new object[Data.Values.Count];
                Data.Values.CopyTo(values, 0);

                for (int i = 0; i < keys.Length; i++)
                {
                    // if the object is a Dictionary<object,object>
                    // convert into MaxSerializedObject
                    if (values[i].GetType() == typeof(Dictionary<object, object>))
                    {
                        values[i] = CleanObject((Dictionary<object, object>)values[i]);
                    }
                    // if the object is a Dictionary<string, object>
                    // clean its data normally
                    else if (values[i].GetType() == typeof(Dictionary<string, object>))
                    {
                        values[i] = CleanData((Dictionary<string, object>)values[i]);
                    }
                    // if the object is anything else, clean the object
                    else
                    {
                        values[i] = CleanObject(values[i]);
                    }
                    // replace the Data with the cleaned object
                    Data[keys[i]] = values[i];
                }
            }
            return new MaxSerializedObject(Data);
        }

        // Cleans a single object
        private static object CleanObject(object obj)
        {
            
            if (obj != null)
            {
                //if a list, convert to array.
                if (obj.GetType() == typeof(List<object>))
                {
                    List<object> obje = (List<object>)obj;
                    for (int i = 0; i < obje.Count; i++)
                    {
                        obje[i] = CleanObject(obje[i]);
                    }
                    obj = obje.ToArray();
                }
                //if object,object dictionary, convert to string,object dictionary, and clean
                else if (obj.GetType() == typeof(Dictionary<object, object>))
                {
                    Dictionary<object, object> obje = (Dictionary<object, object>)obj;
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    foreach (KeyValuePair<object, object> oj in obje)
                    {
                        dic.Add((string)oj.Key, CleanObject(oj.Value));
                    }

                    obj = dic;
                }
                // if object is a string
                if (obj.GetType() == typeof(string))
                {
                    string test = (string)obj;
                    // test to see if the object is actually a boolean.
                    // set it to a boolean value if it is either "true" or "false"
                    if (test == "true")
                    {
                        obj = true;
                    }
                    else if (test == "false")
                    {
                        obj = false;
                    }
                }
            }

            return obj;
        }

        #endregion
    }
}
